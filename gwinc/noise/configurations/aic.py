#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 19 14:39:29 2019

@author: stefan

This file contains modules for calculation of Quantum Noise transfer functions for different Advanced Interferometer Configurations.
It has to be called from the quantum.py module within the corresponding shotradNNN() function with NNN standing for the name of the configuration.
"""

from __future__ import division
from numpy import pi, sqrt, arctan, tan, sin, cos, exp, size, ones, zeros, log10, conj, sum
import numpy as np
import scipy.constants


###########################################
# Tools for matrix manipulations with TFs #
###########################################


def make2x2TF(A11, A12, A21, A22):
    """
    Create transfer matrix with 2x2xnF.
    """
    #now using numpy with broadcasting
    A11, A12, A21, A22 = np.broadcast_arrays(A11, A12, A21, A22)
    M3 = np.array([[A11, A12], [A21, A22]])
    return M3.reshape(2, 2, -1)         

def getProdTF(lhs, rhs):
    """Compute the product of M Nout x Nin x Naf frequency dependent transfer matrices

    See also getTF.

    NOTE: To perform more complicated operations on transfer
          matrices, see LTI object FRD ("help frd").  This
          function is the same as: freqresp(frd(lhs) * frd(rhs), f)

    """
    # check matrix size
    if lhs.shape[1] != rhs.shape[0]:
        raise Exception('Matrix size mismatch size(lhs, 2) = %d != %d = size(rhs, 1)' % (lhs.shape[1], rhs.shape[0]))
    N = lhs.shape[0]
    M = rhs.shape[1]
    if len(lhs.shape) == 3:
        lhs = np.transpose(lhs, axes=(2, 0, 1))
    if len(rhs.shape) == 3:
        rhs = np.transpose(rhs, axes=(2, 0, 1))

    # compute product
    if len(lhs.shape) < 3 or lhs.shape[0] == 1:
        rslt = np.matmul(lhs, rhs)
    elif len(rhs.shape) < 3 or rhs.shape[0] == 1:
        rslt = np.matmul(lhs, rhs)
    elif lhs.shape[0] == rhs.shape[0]:
        rslt = np.matmul(lhs, rhs)
    else:
        raise Exception('Matrix size mismatch lhs.shape[2] = %d != %d = rhs.shape[2]' % (lhs.shape[2], rhs.shape[2]))

    if len(rslt.shape) == 3:
        rslt = np.transpose(rslt, axes=(1, 2, 0))
    return rslt

def invTF(A):
    """
    Calculates inverse of the frequency-dependent transfer matrix of shape (Nout,Nin,Naf)
    """
    # Check matrix dimensions for consistency
    if A.shape[0]!=A.shape[1]:
        raise Exception('Transfer matrix is not square: Nout = %d != %d = Nin!' % (A.shape[0],A.shape[1]))
    else:
        invA = np.linalg.inv(A.transpose(2,0,1)).transpose(1,2,0)
    return invA 

########################################
# Fabry-Perot arm TF
########################################\
def TF_arm(f,ifo):
    """
    Transfer matrix for a single arm. 

    New version Nov 2018 by S.Danilishin based on transfer function formalism

    Mifo    = arm input-output relation for reflected field
    Msig    = GW signal transfer function 
    Mn      = arm input-output relation for transmitted loss field
    
    """    
    # f                                          # Signal Freq. [Hz]
    lambda_ = ifo.Laser.Wavelength               # Laser Wavelength [m]
    hbar    = scipy.constants.hbar               # Plancks Constant [Js]
    c       = scipy.constants.c                  # SOL [m/s]
    omega_0 = 2*pi*c/lambda_                     # Laser angular frequency [rads/s]

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    L       = ifo.Infrastructure.Length          # Length of arm cavities [m]
    T_ITM   = ifo.Optics.ITM.Transmittance       # ITM Transmittance [Power]
    T_ETM   = ifo.Optics.ETM.Transmittance       # ETM Transmittance that accounts for loss[Power]
    m       = ifo.Materials.MirrorMass           # Mirror mass [kg]
    P = ifo.gwinc.parm                           # use precomputed value  of Power circulating in the arm
    print('Arm Circulating Power:            %7.2f Watt' % P)
    print('Mirror mass:                      %7.2f Watt' % m)
    # Derived values
    R_ITM   = 1 - T_ITM
    R_ETM   = 1 - T_ETM
    rETM    = sqrt(R_ETM)
    tETM    = sqrt(T_ETM)
    rITM    = sqrt(R_ITM)
    tITM    = sqrt(T_ITM)
        
    nf = len(f)
    Omega   = 2*pi*f             # Signal angular frequency [rad/s]

    ID = np.array([[np.ones(nf), np.zeros(nf)], [np.zeros(nf), np.ones(nf)]])

    # Reserve memory for transfer matrices for dark port input and signal field
    Mifo = zeros((2,2,nf), dtype=complex)
    Msig = zeros((2,1,nf), dtype=complex)
    MifoBA = zeros((2,2,nf), dtype=complex)
    

    # transfer matrices for SEC and arm loss fields
    Mp = zeros((2,2,nf), dtype=complex)
    Mn = zeros((2,2,nf), dtype=complex)
    MnBA = zeros((2,2,nf), dtype=complex)

    # Rotation matrix
#    RR = np.array([[np.tile(cos(phi), nf), np.tile(sin(phi), nf)],
#                    [np.tile(-sin(phi), nf), np.tile(cos(phi), nf)]])
    
    h_SQL   = sqrt(8 * hbar / (m * (Omega * L)**2))     # SQL Strain
    Kfm     = 8 * P * omega_0 / (m * c**2 * Omega**2) # Free mass Kimble factor
    e2j     = exp(2j*Omega*L/c) # Dispersion phase coeff of the distance 2L
    e1j     = exp(1j*Omega*L/c) # Dispersion phase coeff of the distance L
    cos2    = cos(2*Omega*L/c) # 
    
    Karm    = rETM * T_ITM * (R_ETM+R_ITM) * Kfm / (1-2*rITM*cos2+R_ITM) # Kimble factor of the Fabry-Perot cavity
   
#    Karm    =e2j * rETM * T_ITM * (R_ETM+R_ITM) * Kfm / ((rITM - e2j*rETM)*(e2j * rETM *rITM - 1))
    betaarm = arctan((1+rITM)*tan(Omega*L/c)/(1-rITM)) # frequency-dependent phase of sidebands, reflected off the arm

#    Narm    = (R_ETM + e2j*rETM*rITM**3) * Kfm / (e2j*rETM*rITM - 1) # Kimble factor for the loss fields entering the arm from the ETM
    Narm    = sqrt(Karm * Kfm * R_ETM / (1 - R_ITM**2)) * exp(1j * (betaarm-Omega*L/c)) * (1 + sqrt(R_ITM)**3 * exp(2j * Omega*L/c)) # Kimble factor for the loss fields entering the arm from the ETM
    coef_TT_arm = exp(2j*betaarm) # Overall factor in front of the tuned arm TM for incident field
    coef_NN_arm = exp(1j*betaarm)*sqrt((1-R_ETM) / (1+R_ITM) * (Karm / Kfm)) # Overall factor in front of the tuned arm TM for loss field
#    coef_TT_arm = (rITM - rETM*e2j)/(e2j*rITM - 1) # Overall factor in front of the tuned arm TM for incident field
#    coef_NN_arm = - e1j * tITM*tETM/(e2j*rETM*rITM - 1) # Overall factor in front of the tuned arm TM for loss field
    
    TT_arm_cc   = coef_TT_arm # Tuned arm reflection TM cc-component
    TT_arm_cs   = zeros(nf) # Tuned arm reflection TM cs-component
    TT_arm_sc   = -coef_TT_arm * Karm # Tuned arm reflection TM sc-component, containing back-action term
    TT_arm_ss   = coef_TT_arm # Tuned arm reflection TM ss-component
#    
    NN_arm_cc   = coef_NN_arm # Tuned arm transmission TM cc-component
    NN_arm_cs   = zeros(nf) # Tuned arm transmission TM cs-component
    NN_arm_sc   = -coef_NN_arm * Narm # Tuned arm reflection TM sc-component, containing back-action term
    NN_arm_ss   = coef_NN_arm # Tuned arm transmission TM ss-component
    
#    Back-action-only components of transfer matrices
    TT_arm_BA_cc   = zeros(nf) # 
    TT_arm_BA_cs   = zeros(nf) # 
    TT_arm_BA_sc   = -coef_TT_arm * Karm # Tuned arm reflection TM sc-component, containing back-action term
    TT_arm_BA_ss   = zeros(nf) # 

    NN_arm_BA_cc   = zeros(nf) # 
    NN_arm_BA_cs   = zeros(nf) # 
    NN_arm_BA_sc   = -coef_NN_arm * Narm # Tuned arm transmission TM sc-component, containing back-action term
    NN_arm_BA_ss   = zeros(nf) # 
    
#    TT_arm_cc   = ones(nf) # Tuned arm reflection TM cc-component
#    TT_arm_cs   = zeros(nf) # Tuned arm reflection TM cs-component
#    TT_arm_sc   = zeros(nf) # Tuned arm reflection TM sc-component, containing back-action term
#    TT_arm_ss   = ones(nf) # Tuned arm reflection TM ss-component
#    
#    NN_arm_cc   = zeros(nf) # Tuned arm reflection TM cc-component
#    NN_arm_cs   = zeros(nf) # Tuned arm reflection TM cs-component
#    NN_arm_sc   = zeros(nf) # Tuned arm reflection TM sc-component, containing back-action term
#    NN_arm_ss   = zeros(nf) # Tuned arm reflection TM ss-component
    
    t_arm   = sqrt(2) * e1j * sqrt(R_ETM * T_ITM *Kfm) / (1 - e2j * rETM * rITM) # SQL-normalised response to the arm length change 
#    t_arm   = ones(nf) 

#   Transfer matrices of the arm for reflected and transmitted field     
    Mifo    = make2x2TF(TT_arm_cc,TT_arm_cs,TT_arm_sc,TT_arm_ss) # tuned arm TM for incident field
    Mn      = make2x2TF(NN_arm_cc,NN_arm_cs,NN_arm_sc,NN_arm_ss) # tuned arm TM for loss field
    
#   Back-action-only transfer matrices
    MifoBA    = make2x2TF(TT_arm_BA_cc,TT_arm_BA_cs,TT_arm_BA_sc,TT_arm_BA_ss) # tuned arm TM for incident field
    MnBA      = make2x2TF(NN_arm_BA_cc,NN_arm_BA_cs,NN_arm_BA_sc,NN_arm_BA_ss) # tuned arm TM for loss field
 
#   Response of the arm    
    Msig    = np.array([[np.zeros(nf)], [t_arm / h_SQL]]) # tuned arm response vector to GW strain h  
#    print(t_arm / h_SQL)
    return Mifo, Msig, Mn, MifoBA, MnBA    

def TF_arm_parth(f,ifo):
    """
    Transfer matrix for a single arm. 

    New(er) version June 2019 by P.Nayak based on transfer function formalism with general equations

    Mifo    = arm input-output relation for reflected field
    Msig    = GW signal transfer function 
    Mn      = arm input-output relation for transmitted loss field
    
    """    
    # f                                          # Signal Freq. [Hz]
    lambda_ = ifo.Laser.Wavelength               # Laser Wavelength [m]
    hbar    = scipy.constants.hbar               # Plancks Constant [Js]
    #print(hbar)
    c       = scipy.constants.c                  # SOL [m/s]
    omega_0 = 2*pi*c/lambda_                     # Laser angular frequency [rads/s]

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    L       = ifo.Infrastructure.Length          # Length of arm cavities [m]
    T_ITM   = ifo.Optics.ITM.Transmittance       # ITM Transmittance [Power]
    T_ETM   = ifo.Optics.ETM.Transmittance       # ETM Transmittance that accounts for loss[Power]
    m       = ifo.Materials.MirrorMass / 2.           # Mirror mass [kg]
    P = ifo.gwinc.parm                           # use precomputed value  of Power circulating in the arm
    print('Arm Circulating Power:            %7.2f Watt' % P)
    # Derived values
    R_ITM   = 1 - T_ITM
    R_ETM   = 1 - T_ETM
    rETM    = sqrt(R_ETM)
    tETM    = sqrt(T_ETM)
    rITM    = sqrt(R_ITM)
    tITM    = sqrt(T_ITM)
        
    nf = len(f)
    #print("nf =", nf)
    Omega   = 2*pi*f             # Signal angular frequency [rad/s]
    #print("Omega.shape =", Omega.shape)
    Phi     = omega_0 * L / c
    k       = Phi / (2 * pi)
#    Phi_arm = pi / 12.
    #Phi_arm = 2 * pi * (k - int(k))
    Phi_arm = 0.0000000000000001 #ifo.Optics.ETM.ArmTunePhase
    print("Arm tune phase: ", Phi_arm)
    
    
    def Rot(theta):              # Rotation matrix
        return make2x2TF(cos(theta), -sin(theta), sin(theta), cos(theta))

    ID = np.array([[np.ones(nf), np.zeros(nf)], [np.zeros(nf), np.ones(nf)]])
    #print("ID.shape =", ID.shape)

    # Reserve memory for transfer matrices for dark port input and signal field
    Mifo = zeros((2,2,nf), dtype=complex)
    Msig = zeros((2,1,nf), dtype=complex)
    MifoBA = zeros((2,2,nf), dtype=complex)
    

    # transfer matrices for SEC and arm loss fields
    Mp = zeros((2,2,nf), dtype=complex)
    Mn = zeros((2,2,nf), dtype=complex)
    MnBA = zeros((2,2,nf), dtype=complex)

    # Rotation matrix
#    RR = np.array([[np.tile(cos(phi), nf), np.tile(sin(phi), nf)],
#                    [np.tile(-sin(phi), nf), np.tile(cos(phi), nf)]])
    
    h_SQL   = sqrt(8 * hbar / (m * (Omega * L)**2))     # SQL Strain
    #print(h_SQL[55])
    Kfm     = 8 * P * omega_0 / (m * c**2 * Omega**2) # Free mass Kimble factor
    e2j     = exp(2j*Omega*L/c) # Dispersion phase coeff of the distance 2L
    e1j     = exp(1j*Omega*L/c) # Dispersion phase coeff of the distance L
    cos2    = cos(2*Omega*L/c) # 
    
    Karm    = T_ITM * rETM * (R_ETM+R_ITM) * Kfm / (1-2*rITM*cos2+R_ITM) # Kimble factor of the Fabry-Perot cavity
#    Karm    =e2j * rETM * T_ITM * (R_ETM+R_ITM) * Kfm / ((rITM - e2j*rETM)*(e2j * rETM *rITM - 1))
    #print(Karm[0])
    #print(Karm[100])
    betaarm = arctan((1+rITM)*tan(Omega*L/c)/(1-rITM)) # frequency-dependent phase of sidebands, reflected off the arm
    #print(betaarm[20])
    phase   = exp(1j*betaarm)
    N       = sqrt((1-R_ETM)*Karm/((1+R_ITM)*Kfm))
    Narm    = sqrt(Karm*Kfm*R_ETM/(1-R_ITM**2))*(1+e2j*rITM**3)*phase/e1j # Kimble factor for the loss fields entering the arm from the ETM
    #print(Narm[5])
    
    coef_TT_arm = phase**2 # Overall factor in front of the tuned arm TM for incident field
    coef_NN_arm = N*phase # Overall factor in front of the tuned arm TM for loss field
    

    
#    Back-action-only components of transfer matrices
    TT_arm_BA_cc   = zeros(nf) # 
    TT_arm_BA_cs   = zeros(nf) # 
    TT_arm_BA_sc   = -coef_TT_arm * rETM * Karm # Tuned arm reflection TM sc-component, containing back-action term
    TT_arm_BA_ss   = zeros(nf) # 

    NN_arm_BA_cc   = zeros(nf) # 
    NN_arm_BA_cs   = zeros(nf) # 
    NN_arm_BA_sc   = -coef_NN_arm * Narm # Tuned arm transmission TM sc-component, containing back-action term
    NN_arm_BA_ss   = zeros(nf) # 
    
    Mm_ETM = make2x2TF(zeros(nf), zeros(nf), -R_ETM*Kfm, zeros(nf))
    #print("Mm_ETM.shape =", Mm_ETM.shape)
    Tm_ETM = rETM * (ID + Mm_ETM)
    Nm_ETM = tETM * (ID + Mm_ETM)
    tm_ETM = np.array([[np.zeros(nf)], [sqrt(2 * R_ETM * Kfm)]])
    #print("tm_ETM.shape =", tm_ETM.shape)
    
    Mm_ITM = make2x2TF(zeros(nf), zeros(nf), -R_ITM*Kfm, zeros(nf))
    Tm_ITM = rITM * ID #+ Mm_ITM)
    Nm_ITM = tITM * ID #+ Mm_ITM)
    
    #Mm_ETM_Phi   = getProdTF(getProdTF(Rot(Phi), Mm_ETM), Rot(-Phi))
    Tm_ETM_Phi   = getProdTF(getProdTF(Rot(Phi_arm), Tm_ETM), Rot(-Phi_arm))
    Nm_ETM_Phi   = getProdTF(getProdTF(Rot(Phi_arm), Nm_ETM), Rot(-Phi_arm))
    tm_ETM_Phi   = getProdTF(Rot(Phi_arm), tm_ETM)
    
    Tm_ITM_Phi   = getProdTF(getProdTF(Rot(Phi_arm), Tm_ITM), Rot(-Phi_arm))
    Nm_ITM_Phi   = getProdTF(getProdTF(Rot(Phi_arm), Nm_ITM), Rot(-Phi_arm))
    
    P_Phi = e1j * Rot(Phi_arm)
    #print("P_Phi.shape =", P_Phi.shape)
    
#    TT_arm_cc   = ones(nf) # Tuned arm reflection TM cc-component
#    TT_arm_cs   = zeros(nf) # Tuned arm reflection TM cs-component
#    TT_arm_sc   = zeros(nf) # Tuned arm reflection TM sc-component, containing back-action term
#    TT_arm_ss   = ones(nf) # Tuned arm reflection TM ss-component
#    
#    NN_arm_cc   = zeros(nf) # Tuned arm reflection TM cc-component
#    NN_arm_cs   = zeros(nf) # Tuned arm reflection TM cs-component
#    NN_arm_sc   = zeros(nf) # Tuned arm reflection TM sc-component, containing back-action term
#    NN_arm_ss   = zeros(nf) # Tuned arm reflection TM ss-component
    
    #t_arm   = phase * sqrt(2 * R_ETM * Karm / (1 + R_ITM)) # SQL-normalised response to the arm length change
    #print(t_arm[5])
#    t_arm   = ones(nf) 

#   Transfer matrices of the arm for reflected and transmitted field     
    #Mifo    = make2x2TF(TT_arm_cc,TT_arm_cs,TT_arm_sc,TT_arm_ss) # tuned arm TM for incident field
#    Marm    = invTF(ID - getProdTF(getProdTF(P_Phi, Tm_ETM_Phi), getProdTF(P_Phi, Tm_ITM_Phi)))
#    Mifo    = tITM * getProdTF(getProdTF(getProdTF(Marm, P_Phi), getProdTF(Tm_ETM_Phi, P_Phi)), Nm_ITM_Phi) - rITM * ID 
#    #Mn      = make2x2TF(NN_arm_cc,NN_arm_cs,NN_arm_sc,NN_arm_ss) # tuned arm TM for loss field
#    Mn      = tITM * getProdTF(getProdTF(Marm, P_Phi), Nm_ETM_Phi)
    
    Marm    = invTF(ID - getProdTF(getProdTF(P_Phi, Tm_ETM), getProdTF(P_Phi, Tm_ITM)))
    Mifo    = tITM * getProdTF(getProdTF(getProdTF(Marm, P_Phi), getProdTF(Tm_ETM, P_Phi)), Nm_ITM) - rITM * ID 
#    Mn      = make2x2TF(NN_arm_cc,NN_arm_cs,NN_arm_sc,NN_arm_ss) # tuned arm TM for loss field
    Mn      = tITM * getProdTF(getProdTF(Marm, P_Phi), Nm_ETM)
    
#   Back-action-only transfer matrices
    MifoBA    = make2x2TF(TT_arm_BA_cc,TT_arm_BA_cs,TT_arm_BA_sc,TT_arm_BA_ss) # tuned arm TM for incident field
    MnBA      = make2x2TF(NN_arm_BA_cc,NN_arm_BA_cs,NN_arm_BA_sc,NN_arm_BA_ss) # tuned arm TM for loss field
 
#   Response of the arm    
    #Msig    = np.array([[np.zeros(nf)], [t_arm/h_SQL]]) # tuned arm response vector to GW strain h  
#    print(t_arm / h_SQL)
    Msig      = tITM * getProdTF(getProdTF(Marm, P_Phi), tm_ETM) / h_SQL
    print("This is using general equations...")
    #Msig      = sqrt(2) * tITM * tm_ETM_Phi / h_SQL
    
    
    #print("General: Mifo(100) = \n", Mifo[:,:,100])
    
    #Mifo, Mn, Msig = TF_arm_new.TF_arm(f, ifo)
    #M1 = np.transpose(Mifo, axes=(2,0,1))
    #print(M1.shape[0], Mn.shape[-1])
    
    return Mifo, Msig, Mn, MifoBA, MnBA


def TF_MI(f, ifo):
    """
    Calculate transfer matrices for a FP-Michelson interferometer with identical arms and (a)symmetric beam splitter
    """
    Mifo_arm, Msig_arm, Mn_arm, Mifo_arm_BA, Mn_arm_BA  = TF_arm(f,ifo)    
    if 'BSasym' not in ifo.Optics:
        etaBS       = 0 #set BS asymmetry to zero
        # symmetric BS            
        Mifo        = Mifo_arm
        Mn          = Mn_arm
        Msig        = sqrt(2) * Msig_arm
    else:
        etaBS       = ifo.Optics.BSasym      
        # asymmetric BS        
        print(etaBS)
        Mifo        = (1+etaBS**2) * Mifo_arm  +  etaBS**2 * (5+etaBS**2) * Mifo_arm_BA # TM from the dark port to the dark port
        MifoBP      = -2*etaBS * (1-etaBS**2) * Mifo_arm_BA                          # TM from the bright port to the dark port
        Mn          = Mn_arm  +  3 * etaBS**2 * Mn_arm_BA                             # dARM mode
#       MnBP = -etaBS * (NN_arm + 2*(2+etaBS**2)*NN_arm_ba);                   # cARM mode
        Msig        = sqrt(2) * (1+etaBS**2) * Msig_arm  # 
    return Mifo, Msig, Mn

def TF_Sag(f,ifo):
    """
    Calculate transfer matrices for a Sagnac speed-meter interferometer 
    as per Appendix C.3 of Voronchev et al. https://arxiv.org/abs/1503.01062 
    """
    Mifo_arm, Msig_arm, Mn_arm, Mifo_arm_BA, Mn_arm_BA  = TF_arm(f,ifo) 
    
    nf = len(f)
    
    ID = np.array([[np.ones(nf), np.zeros(nf)], [np.zeros(nf), np.ones(nf)]]) #Identity matrix
    M_SI    = zeros((2,2,nf), dtype=complex) # reserve memory for auxiliary Sagnac closed-loop gain matrix 
    Mifo_SI = M_SI
    Mn1_SI = M_SI
    Mn2_SI = M_SI
    
    
    M_SI    = ID - Mifo_arm_BA #auxiliary Sagnac closed-loop gain matrix defined by (C9) of Appendix C.3 of Voronchev et al. https://arxiv.org/abs/1503.01062 
    Mifo_SI = Mifo_arm_BA - getProdTF(getProdTF(Mifo_arm, M_SI),Mifo_arm) # Transfer matrix of a Sagnac IFO as per (C9) of Appendix C.3 of Voronchev et al. https://arxiv.org/abs/1503.01062
    Mn1_SI  = Mn_arm - getProdTF(getProdTF(Mifo_arm, M_SI),Mn_arm_BA) # Transfer matrix for loss fields in the first pass of the beam through Sagnac IFO as per (59) and (C9) of Appendix C.3 of Voronchev et al. https://arxiv.org/abs/1503.01062
    Mn2_SI  = Mn_arm_BA - getProdTF(getProdTF(Mifo_arm, M_SI),Mn_arm) # Transfer matrix for loss fields in the second pass of the beam through Sagnac IFO as per (59) and  (C9) of Appendix C.3 of Voronchev et al. https://arxiv.org/abs/1503.01062
    Mn_sum  = np.hstack((Mn1_SI,Mn1_SI)) # Combine loss-related noise transfer matrices
    Msig_SI = -sqrt(2) * getProdTF((ID - getProdTF(Mifo_arm,M_SI)),Msig_arm) # Response vector of Sagnac IFO
    
    Mifo    = Mifo_SI
    Msig    = Msig_SI
    Mn      = Mn_sum
    coeff   = 1
    return Mifo, Msig, Mn

#def TF_Sag(f,ifo):
#    """
#    Calculate transfer matrices for a Sagnac speed-meter interferometer 
#    as per Appendix C.3 of Voronchev et al. https://arxiv.org/abs/1503.01062 
#    """
#    Mifo_arm, Msig_arm, Mn_arm, Mifo_arm_BA, Mn_arm_BA  = TF_arm(f,ifo) 
#    
#    nf = len(f)
#    
#    ID = np.array([[np.ones(nf), np.zeros(nf)], [np.zeros(nf), np.ones(nf)]]) #Identity matrix
#    M_SI    = zeros((2,2,nf), dtype=complex) # reserve memory for auxiliary Sagnac closed-loop gain matrix 
#    Mifo_SI = M_SI
#    Mn1_SI = M_SI
#    Mn2_SI = M_SI
#    
#    
#    M_SI    = ID - Mifo_arm_BA #auxiliary Sagnac closed-loop gain matrix defined by (C9) of Appendix C.3 of Voronchev et al. https://arxiv.org/abs/1503.01062 
#    Mifo_SI = Mifo_arm_BA - getProdTF(getProdTF(Mifo_arm, M_SI),Mifo_arm) # Transfer matrix of a Sagnac IFO as per (C9) of Appendix C.3 of Voronchev et al. https://arxiv.org/abs/1503.01062
#    Mn1_SI  = Mn_arm - getProdTF(getProdTF(Mifo_arm, M_SI),Mn_arm_BA) # Transfer matrix for loss fields in the first pass of the beam through Sagnac IFO as per (59) and (C9) of Appendix C.3 of Voronchev et al. https://arxiv.org/abs/1503.01062
#    Mn2_SI  = Mn_arm_BA - getProdTF(getProdTF(Mifo_arm, M_SI),Mn_arm) # Transfer matrix for loss fields in the second pass of the beam through Sagnac IFO as per (59) and  (C9) of Appendix C.3 of Voronchev et al. https://arxiv.org/abs/1503.01062
#    Mn_sum  = np.hstack((Mn1_SI,Mn1_SI)) # Combine loss-related noise transfer matrices
#    Msig_SI = -sqrt(2) * getProdTF((ID - getProdTF(Mifo_arm,M_SI)),Msig_arm) # Response vector of Sagnac IFO
#    
#    Mifo    = Mifo_SI
#    Msig    = Msig_SI
#    Mn      = Mn_sum
#    coeff   = 1
#    return Mifo, Msig, Mn

def TF_SRC(f,ifo,TF_conf):
    """
    Calculate transfer matrices for a signal-recycled interferometer 
    as per Appendix C.3 of Voronchev et al. https://arxiv.org/abs/1503.01062 
    """
    Mifo_noSR, Msig_noSR, Mn_noSR = TF_conf(f,ifo)
    nf      = len(f)
    
    Omega   = 2*pi*f
    c       = scipy.constants.c
#    L       = ifo.Infrastructure.Length          # Length of arm cavities [m]
    lSR     = ifo.Optics.SRM.CavityLength        # Length from the ITM to SRM [m]
    phiSR   = (pi-ifo.Optics.SRM.Tunephase)/2    # (NB!:BnC like convention of tunephase = omega_0*lSR/c)Sub-wavelength detuning of SRC [rad]
    TSR     = ifo.Optics.SRM.Transmittance       # SRM Transmittance [Power]
#    RSR     = 1-TSR                              # SRM Transmittance [Power]
    rhoSR   = sqrt(1 - TSR)                        # SRM Transmittance [Amplitude]
#    m       = ifo.Materials.MirrorMass           # Mirror mass [kg]
    alphaSR = arctan(tan(phiSR)*(rhoSR-1)/(rhoSR+1)) # additional scaling law phase
    
    ID      = np.array([[np.ones(nf), np.zeros(nf)], [np.zeros(nf), np.ones(nf)]]) #Identity matrix
    P_SR    = exp(1j*Omega*lSR/c)*make2x2TF(cos(phiSR), -sin(phiSR), sin(phiSR), cos(phiSR)) # Matrix of light propagation in the SRC
    P2_SR   = exp(2j*Omega*lSR/c)*make2x2TF(cos(2*phiSR), -sin(2*phiSR), sin(2*phiSR), cos(2*phiSR)) # Matrix of light round-trip propagation in the SRC
    Rot     = make2x2TF(cos(alphaSR), -sin(alphaSR), sin(alphaSR), cos(alphaSR)) # (Not used!) Scaling law offset -  additional phase to make the I/O-relations of the SR IFO be the same as that of a detuned FP cavity
    
    
    M_SR    = zeros((2,2,nf), dtype=complex) # reserve memory for auxiliary Sagnac closed-loop gain matrix 
    Mifo    = M_SR
    Mn      = M_SR
    Msig    = M_SR
    
    # Transfer matrices and response function of the SR-interferometer given as in appendix B.3.2 of Living Review (2019)22:2 (https://link.springer.com/article/10.1007/s41114-019-0018-y)
    M_SR    = invTF(ID - rhoSR * getProdTF(Mifo_noSR,P2_SR)) # Open-loop gain of the SRC
    Mifo    = TSR * getProdTF(getProdTF(getProdTF(P_SR,M_SR),Mifo_noSR),P_SR) - rhoSR * ID # Transfer matrix of the signal-recycled iFO for dark port field
#    Mifo    = getProdTF(getProdTF(Rot,Mifo),Rot) # Implementing additional phase 
    Mn      = sqrt(TSR) * getProdTF(getProdTF(P_SR,M_SR),Mn_noSR) # Transfer matrix of the signal-recycled iFO for loss-in-the-arms vacuum fields
    Msig    = sqrt(TSR) * getProdTF(getProdTF(P_SR,M_SR),Msig_noSR) # Response of the signal-recycled IFO to gW  strain 
    return Mifo, Msig, Mn