#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 21 14:16:53 2019

Put here functions dealing with mathematical operations on frequency-dependent transfer functions 
(moved here from the original quantum.py)

@author: stefan
"""
import numpy as np

def make2x2TF(A11, A12, A21, A22):
    """
    Create transfer matrix with 2x2xnF.
    """
    #now using numpy with broadcasting
    A11, A12, A21, A22 = np.broadcast_arrays(A11, A12, A21, A22)
    M3 = np.array([[A11, A12], [A21, A22]])
    return M3.reshape(2, 2, -1)         

def getProdTF(lhs, rhs):
    """Compute the product of M Nout x Nin x Naf frequency dependent transfer matrices

    See also getTF.

    NOTE: To perform more complicated operations on transfer
          matrices, see LTI object FRD ("help frd").  This
          function is the same as: freqresp(frd(lhs) * frd(rhs), f)

    """
    # check matrix size
    if lhs.shape[1] != rhs.shape[0]:
        raise Exception('Matrix size mismatch size(lhs, 2) = %d != %d = size(rhs, 1)' % (lhs.shape[1], rhs.shape[0]))
    N = lhs.shape[0]
    M = rhs.shape[1]
    if len(lhs.shape) == 3:
        lhs = np.transpose(lhs, axes=(2, 0, 1))
    if len(rhs.shape) == 3:
        rhs = np.transpose(rhs, axes=(2, 0, 1))

    # compute product
    if len(lhs.shape) < 3 or lhs.shape[0] == 1:
        rslt = np.matmul(lhs, rhs)
    elif len(rhs.shape) < 3 or rhs.shape[0] == 1:
        rslt = np.matmul(lhs, rhs)
    elif lhs.shape[0] == rhs.shape[0]:
        rslt = np.matmul(lhs, rhs)
    else:
        raise Exception('Matrix size mismatch lhs.shape[2] = %d != %d = rhs.shape[2]' % (lhs.shape[2], rhs.shape[2]))

    if len(rslt.shape) == 3:
        rslt = np.transpose(rslt, axes=(1, 2, 0))
    return rslt

def invTF(A):
    """
    Calculates inverse of the frequency-dependent transfer matrix of shape (Nout,Nin,Naf)
    """
    # Check matrix dimensions for consistency
    if A.shape[0]!=A.shape[1]:
        raise Exception('Transfer matrix is not square: Nout = %d != %d = Nin!' % (A.shape[0],A.shape[1]))
    else:
        invA = np.linalg.inv(A.transpose(2,0,1)).transpose(1,2,0)
    return invA 