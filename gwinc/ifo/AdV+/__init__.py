from gwinc.ifo.noises import *
from gwinc.ifo import PLOT_STYLE


class AdVplus(nb.Budget):

    name = 'Advanced Virgo+'

    noises = [
        QuantumVacuum,
        Seismic,
        Newtonian,
        SuspensionThermal,
        CoatingBrownian,
        CoatingThermoOptic,
        SubstrateBrownian,
        SubstrateThermoElastic,
        ExcessGas,
    ]

    calibrations = [
        Strain,
    ]

    plot_style = PLOT_STYLE
